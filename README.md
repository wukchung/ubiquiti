# README #

Implementation based on requirements from Ubiquity.

## Programovací část ##
```
./assets
./init
./src
./index.php
./result.html
```
Kromě CSS a Loger Interface dle PSR neobsahuje žádné komponenty či kód třetích stran. Nezaměřoval jsem na "podhoubí" aplikace ale spíše na samotnout realizaci HTML elementů. V **result.html** je uložen výsledek běho scriptu, který je třeba otevřít se všemi ostatními soubory aby spravně načetla CSS a assety.

## Databázová část ##
```
./SQL
```
Dotaz pro nejdelší skladbu jsem udělal tak aby vybral všechny nejdelší skladby pokud je jich více stejné délky.


