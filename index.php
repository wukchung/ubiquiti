<?php

use \Ubiquiti\Html\ElementFactory;

define('ROOT_DIR', __DIR__ . DIRECTORY_SEPARATOR);

include_once(ROOT_DIR . 'init' . DIRECTORY_SEPARATOR . 'base.php');

ob_start();

try {
    // create a basic document
    $document = ElementFactory::getDocument();

    // main div container
    $div = ElementFactory::getDiv()->setClass('main');
    $document->addChild($div);

    // logo with link
    $link = ElementFactory::getLink()->setHref('https://www.ubnt.com/')->addChild(ElementFactory::getImage()->setSrc('assets/image/logo.gif'));
    $div->addChild($link);

    // form container
    $form = ElementFactory::getForm()->setAction('target.php')->setClass('basic-grey');
    $div->addChild($form);

    // input for name
    $spanName = ElementFactory::getSpan()->addChild(ElementFactory::getText('Jméno'));
    $inputName = ElementFactory::getInput()->setName('name')->setType('text');
    $labelName = ElementFactory::getLabel()->addChild($spanName)->addChild($inputName);
    $form->addChild($labelName);

    // input for surname
    $spanSurname = ElementFactory::getSpan()->addChild(ElementFactory::getText('Příjmení'));
    $inputSurname = ElementFactory::getInput()->setName('name')->setType('text');
    $labelSurname = ElementFactory::getLabel()->addChild($spanSurname)->addChild($inputSurname);
    $form->addChild($labelSurname);

    // input for surname
    $spanGender = ElementFactory::getSpan()->addChild(ElementFactory::getText('Pohlaví'));
    $optionM = ElementFactory::getOption()->setValue('male')->addChild(ElementFactory::getText('Muž'));
    $optionF = ElementFactory::getOption()->setValue('female')->addChild(ElementFactory::getText('Žena'));
    $selectGender = ElementFactory::getSelect()->setName('gender')->addChild($optionM)->addChild($optionF);
    $labelGender = ElementFactory::getLabel()->addChild($spanGender)->addChild($selectGender);
    $form->addChild($labelGender);

    // submit
    $inputSubmit = ElementFactory::getInput()->setType('submit')->setClass('button')->setValue('Odeslat');
    $labelSubmit = ElementFactory::getLabel()->addChild($inputSubmit);
    $form->addChild($labelSubmit);

    // add paragraph with link
    $link = ElementFactory::getLink()->setHref('https://www.ubnt.com/')->addChild(ElementFactory::getText('Homepage'));
    $div->addChild(ElementFactory::getParagraph()->addChild($link));

    echo $document->render();
}
catch (\Exception $e) {

    ob_end_clean();
    ob_start();

    \Ubiquiti\Logger\LoggerFactory::get()->alert($e->getMessage());
}

ob_end_flush();

