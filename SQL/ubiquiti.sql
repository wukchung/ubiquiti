-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 24, 2017 at 04:35 PM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 7.0.13-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ubiquiti`
--

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `id_album` int(10) UNSIGNED NOT NULL,
  `id_typ_zanr` int(10) UNSIGNED NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `datum_vydani` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id_album`, `id_typ_zanr`, `nazev`, `datum_vydani`) VALUES
(1, 2, 'Apage Satanas', '1999-01-12'),
(2, 1, 'Sehnsucht', '1997-12-04'),
(3, 1, 'Painkiller', '1999-01-04');

-- --------------------------------------------------------

--
-- Table structure for table `album_interpret`
--

CREATE TABLE `album_interpret` (
  `id_album_interpret` int(10) UNSIGNED NOT NULL,
  `id_album` int(10) UNSIGNED NOT NULL,
  `id_interpret` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `album_interpret`
--

INSERT INTO `album_interpret` (`id_album_interpret`, `id_album`, `id_interpret`) VALUES
(1, 1, 2),
(2, 2, 3),
(3, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `album_skladba`
--

CREATE TABLE `album_skladba` (
  `id_album_skladba` int(10) UNSIGNED NOT NULL,
  `cislo_stopy` int(11) NOT NULL,
  `id_album` int(10) UNSIGNED NOT NULL,
  `id_skladba` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `album_skladba`
--

INSERT INTO `album_skladba` (`id_album_skladba`, `cislo_stopy`, `id_album`, `id_skladba`) VALUES
(4, 5, 1, 1),
(5, 7, 2, 2),
(6, 11, 3, 3),
(7, 3, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `interpret`
--

CREATE TABLE `interpret` (
  `id_interpret` int(10) UNSIGNED NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `id_typ_narodnosti` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `interpret`
--

INSERT INTO `interpret` (`id_interpret`, `nazev`, `id_typ_narodnosti`) VALUES
(1, 'Judast Priest', 3),
(2, 'Arakain', 1),
(3, 'Rammstein', 2);

-- --------------------------------------------------------

--
-- Table structure for table `skladba`
--

CREATE TABLE `skladba` (
  `id_skladba` int(10) UNSIGNED NOT NULL,
  `nazev` text COLLATE utf8_czech_ci NOT NULL,
  `delka` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `skladba`
--

INSERT INTO `skladba` (`id_skladba`, `nazev`, `delka`) VALUES
(1, 'Kyborg', 178),
(2, 'Buch dich', 90),
(3, 'Painkiller', 210),
(4, 'Prominte slecno', 300);

-- --------------------------------------------------------

--
-- Table structure for table `typ_narodnost`
--

CREATE TABLE `typ_narodnost` (
  `id_typ_narodnost` int(10) UNSIGNED NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `typ_narodnost`
--

INSERT INTO `typ_narodnost` (`id_typ_narodnost`, `nazev`) VALUES
(1, 'CZE'),
(2, 'GER'),
(3, 'ITA');

-- --------------------------------------------------------

--
-- Table structure for table `typ_zanr`
--

CREATE TABLE `typ_zanr` (
  `id_typ_zanr` int(10) UNSIGNED NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `typ_zanr`
--

INSERT INTO `typ_zanr` (`id_typ_zanr`, `nazev`) VALUES
(1, 'Rock'),
(2, 'Pop'),
(3, 'Blues');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id_album`),
  ADD KEY `zanrToAlbum` (`id_typ_zanr`);

--
-- Indexes for table `album_interpret`
--
ALTER TABLE `album_interpret`
  ADD PRIMARY KEY (`id_album_interpret`),
  ADD KEY `album-interpret` (`id_interpret`),
  ADD KEY `interpret-album` (`id_album`);

--
-- Indexes for table `album_skladba`
--
ALTER TABLE `album_skladba`
  ADD PRIMARY KEY (`id_album_skladba`),
  ADD KEY `album_skladba` (`id_skladba`),
  ADD KEY `skladba_album` (`id_album`);

--
-- Indexes for table `interpret`
--
ALTER TABLE `interpret`
  ADD PRIMARY KEY (`id_interpret`),
  ADD KEY `interpret_narodnost` (`id_typ_narodnosti`);

--
-- Indexes for table `skladba`
--
ALTER TABLE `skladba`
  ADD PRIMARY KEY (`id_skladba`),
  ADD KEY `delka` (`delka`);

--
-- Indexes for table `typ_narodnost`
--
ALTER TABLE `typ_narodnost`
  ADD PRIMARY KEY (`id_typ_narodnost`);

--
-- Indexes for table `typ_zanr`
--
ALTER TABLE `typ_zanr`
  ADD PRIMARY KEY (`id_typ_zanr`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id_album` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `album_interpret`
--
ALTER TABLE `album_interpret`
  MODIFY `id_album_interpret` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `album_skladba`
--
ALTER TABLE `album_skladba`
  MODIFY `id_album_skladba` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `interpret`
--
ALTER TABLE `interpret`
  MODIFY `id_interpret` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `skladba`
--
ALTER TABLE `skladba`
  MODIFY `id_skladba` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `typ_narodnost`
--
ALTER TABLE `typ_narodnost`
  MODIFY `id_typ_narodnost` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `typ_zanr`
--
ALTER TABLE `typ_zanr`
  MODIFY `id_typ_zanr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `album_interpret`
--
ALTER TABLE `album_interpret`
  ADD CONSTRAINT `album-interpret` FOREIGN KEY (`id_interpret`) REFERENCES `interpret` (`id_interpret`),
  ADD CONSTRAINT `interpret-album` FOREIGN KEY (`id_album`) REFERENCES `album` (`id_album`);

--
-- Constraints for table `album_skladba`
--
ALTER TABLE `album_skladba`
  ADD CONSTRAINT `album_skladba` FOREIGN KEY (`id_skladba`) REFERENCES `skladba` (`id_skladba`),
  ADD CONSTRAINT `skladba_album` FOREIGN KEY (`id_album`) REFERENCES `album` (`id_album`);

--
-- Constraints for table `interpret`
--
ALTER TABLE `interpret`
  ADD CONSTRAINT `interpret_narodnost` FOREIGN KEY (`id_typ_narodnosti`) REFERENCES `typ_narodnost` (`id_typ_narodnost`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
