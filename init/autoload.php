<?php

// autoload function
spl_autoload_register('psrAutoload');

/**
 * Autoload function follows PSR-1 definition
 *
 * @param $class
 * @return bool
 */
function psrAutoload($class)
{
    $file = $class . '.php';

    $basePath = ROOT_DIR . 'src' . DIRECTORY_SEPARATOR;
    $filename = $basePath . str_replace('\\', '/', $file);

    if (file_exists($filename)) {
        require_once($filename);
        return true;
    }

    return false;
}

?>