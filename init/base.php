<?php

// configure error reporting
error_reporting(E_ALL);
ini_set('display_errors', true);

// autoload
require_once(ROOT_DIR . 'init' . DIRECTORY_SEPARATOR . 'autoload.php');

?>