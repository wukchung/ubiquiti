<?php

namespace Ubiquiti\Html;

use Ubiquiti\Html\Exception\MissingTemplateException;
use Ubiquiti\Html\Exception\NoParentException;
use Ubiquiti\Html\Tool\Path;

abstract class Element implements IElement
{
    /** @var Element */
    protected $parent;

    /** @var Element[] */
    protected $children = array();

    /** @var array */
    protected $attributes = array();

    /**
     * @return Element
     */
    public function getParent()
    {
        if (empty($this->parent)) {
            throw new NoParentException();
        }

        return $this->parent;
    }

    /**
     * @param Element $parent
     */
    protected function setParent(Element $parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return Element[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param Element $child
     * @return $this
     */
    public function addChild(Element $child)
    {
        $this->children[] = $child;

        $child->setParent($this);

        return $this;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param $attribute
     * @return array
     */
    public function getAttribute($attribute)
    {
        return isset($this->attributes[$attribute]) ? $this->attributes[$attribute] : false;
    }


    /**
     * @param $key
     * @param $attribute
     */
    public function addAttribute($key, $attribute)
    {
        $this->attributes[$key] = $attribute;
    }

    /**
     * Get attributes ready for the injection to the template
     */
    private function getSerializedAttributes()
    {
        $keyValuePair = array();
        foreach ($this->attributes as $key => $att) {
            $keyValuePair[] .= "$key=\"$att\"";
        }

        return implode(' ', $keyValuePair);
    }

    /**
     * @param array $templateVars
     * @return mixed
     */
    public function render($templateVars = array())
    {
        $template = $this->loadTemplate();

        if (!isset($templateVars['attrs'])) $templateVars['attrs'] = $this->getSerializedAttributes();
        if (!isset($templateVars['children'])) $templateVars['children'] = $this->renderChildren();
        return $this->replaceTemplateVars($template, $templateVars);
    }

    /**
     * Replaces given vars. Vars in template has to be embedded in brackets {{<var>}}
     *
     * @param $template
     * @param $templateVars
     * @return mixed
     */
    protected function replaceTemplateVars($template, $templateVars)
    {
        foreach ($templateVars as $key => $var) {
            $template = str_replace('{{' . $key . '}}', $var, $template);
        }

        return $template;
    }

    /**
     * Get rendered content of all children
     *
     * @return string
     */
    private function renderChildren()
    {
        $result = '';
        $children = $this->getChildren();
        foreach ($children as $child) {
            $result .= $child->render();
        }

        return $result;
    }

    /**
     * Load template for elements
     *
     * @return string
     */
    protected function loadTemplate()
    {
        $filename = Path::glue(ROOT_DIR, 'src', 'Ubiquiti', 'Html', 'Element', 'template', $this->getTemplate());

        if (!file_exists($filename)) {
            throw new MissingTemplateException("Template $filename is missing");
        }

        return file_get_contents($filename);
    }

    /**
     * Get filename for element template
     *
     * @return mixed
     */
    abstract protected function getTemplate();
}