<?php

namespace Ubiquiti\Html;

interface IElement
{
    /**
     * @return mixed
     */
    public function render();
}