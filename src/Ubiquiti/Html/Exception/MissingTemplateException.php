<?php

namespace Ubiquiti\Html\Exception;

class MissingTemplateException extends \UnderflowException
{
}