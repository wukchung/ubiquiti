<?php

namespace Ubiquiti\Html\Element;

class Option extends Base
{
    const ATT_VALUE = 'value';

    /**
     * @param $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->addAttribute(self::ATT_VALUE, $value);

        return $this;
    }

    /**
     */
    public function getValue()
    {
        $this->getAttribute(self::ATT_VALUE);
    }

    /**
     * Get filename for element template
     *
     * @return mixed
     */
    protected function getTemplate()
    {
        return 'option.html';
    }
}