<?php

namespace Ubiquiti\Html\Element;

class Label extends Base
{
    /**
     * Get filename for element template
     *
     * @return mixed
     */
    protected function getTemplate()
    {
        return 'label.html';
    }
}