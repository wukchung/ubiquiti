<?php

namespace Ubiquiti\Html\Element;

class Link extends Base
{
    const ATT_HREF = 'href';
    const ATT_ANCHOR = 'anchor';

    /**
     * @param $href
     * @return $this
     */
    public function setHref($href)
    {
        $this->addAttribute(self::ATT_HREF, $href);

        return $this;
    }

    /**
     */
    public function getHref()
    {
        $this->getAttribute(self::ATT_HREF);
    }

    /**
     * @param $anchor
     * @return $this
     */
    public function setAnchor($anchor)
    {
        $this->addAttribute(self::ATT_ANCHOR, $anchor);

        return $this;
    }

    /**
     */
    public function getAnchor()
    {
        $this->getAttribute(self::ATT_ANCHOR);
    }

    /**
     * Get filename for element template
     *
     * @return mixed
     */
    protected function getTemplate()
    {
        return 'link.html';
    }
}