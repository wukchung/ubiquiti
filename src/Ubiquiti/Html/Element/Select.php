<?php

namespace Ubiquiti\Html\Element;

use Ubiquiti\Html\Element;

class Select extends Base
{
    /**
     * @param mixed $name
     * @return $this
     */
    public function setName($name)
    {
        $this->addAttribute(Input::ATT_NAME, $name);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->getAttribute(Input::ATT_NAME);
    }

    /**
     * Get filename for element template
     *
     * @return mixed
     */
    protected function getTemplate()
    {
        return 'select.html';
    }

    /**
     * @param Element $child
     * @return $this|void
     */
    public function addChild(Element $child)
    {
        if (!$child instanceof Option) {
            throw new \InvalidArgumentException('Select can acccept only Option object as children');
        }

        return parent::addChild($child);
    }
}