<?php

namespace Ubiquiti\Html\Element;

class Image extends Base
{
    const ATT_SRC = 'src';

    /**
     * @param mixed $src
     * @return $this
     */
    public function setSrc($src)
    {
        $this->addAttribute(self::ATT_SRC, $src);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSrc()
    {
        return $this->getAttribute(self::ATT_SRC);
    }

    /**
     * Get filename for element template
     *
     * @return mixed
     */
    protected function getTemplate()
    {
        return 'image.html';
    }
}