<?php

namespace Ubiquiti\Html\Element;

class Form extends Base
{
    const ATT_ACTION = 'action';


    /**
     * @param mixed $action
     * @return $this
     */
    public function setAction($action)
    {
        $this->addAttribute(self::ATT_ACTION, $action);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->getAttribute(self::ATT_ACTION);
    }

    /**
     * Get filename for element template
     *
     * @return mixed
     */
    protected function getTemplate()
    {
        return 'form.html';
    }
}