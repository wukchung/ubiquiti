<?php

namespace Ubiquiti\Html\Element;

class Paragraph extends Base
{
    /**
     * Get filename for element template
     *
     * @return mixed
     */
    protected function getTemplate()
    {
        return 'paragraph.html';
    }
}