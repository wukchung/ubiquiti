<?php

namespace Ubiquiti\Html\Element;

class Div extends Base
{
    /**
     * Get filename for element template
     *
     * @return mixed
     */
    protected function getTemplate()
    {
        return 'div.html';
    }
}