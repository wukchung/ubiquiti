<?php

namespace Ubiquiti\Html\Element;

class Span extends Base
{
    /**
     * Get filename for element template
     *
     * @return mixed
     */
    protected function getTemplate()
    {
        return 'span.html';
    }
}