<?php

namespace Ubiquiti\Html\Element;

use Ubiquiti\Html\Element;

class Text extends Element
{
    protected $text;

    public function __construct($text)
    {
        $this->setText($text);
    }

    /**
     * @param $text
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param array $templateVars
     * @return mixed
     */
    public function render($templateVars = array())
    {
        return parent::render(['content' => $this->getText()]);
    }

    /**
     * Get filename for element template
     *
     * @return mixed
     */
    protected function getTemplate()
    {
        return 'text.html';
    }
}