<?php

namespace Ubiquiti\Html\Element;

use Ubiquiti\Html\Element;

class Document extends Element
{
    /**
     * Get filename for element template
     *
     * @return mixed
     */
    protected function getTemplate()
    {
        return 'document.html';
    }
}