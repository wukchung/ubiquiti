<?php

namespace Ubiquiti\Html\Element;

use Ubiquiti\Html\Element;

abstract class Base extends Element
{
    const ATT_CLASS = 'class';
    const ATT_ID = 'id';

    /**
     * @param $class
     * @return $this
     */
    public function setClass($class)
    {
        $this->addAttribute(self::ATT_CLASS, $class);

        return $this;
    }

    /**
     */
    public function getClass()
    {
        $this->getAttribute(self::ATT_CLASS);
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->addAttribute(self::ATT_ID, $id);

        return $this;
    }

    /**
     */
    public function getId()
    {
        $this->getAttribute(self::ATT_ID);
    }
}