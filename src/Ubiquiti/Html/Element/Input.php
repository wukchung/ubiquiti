<?php

namespace Ubiquiti\Html\Element;

class Input extends Base
{
    const ATT_TYPE = 'type';
    const ATT_VALUE = 'value';
    const ATT_NAME = 'name';

    /**
     * @param $type
     * @return $this
     */
    public function setType($type)
    {
        $this->addAttribute(self::ATT_TYPE, $type);

        return $this;
    }

    /**
     */
    public function getType()
    {
        return $this->getAttribute(self::ATT_TYPE);
    }

    /**
     * @param $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->addAttribute(self::ATT_VALUE, $value);

        return $this;
    }

    /**
     */
    public function getValue()
    {
        return $this->getAttribute(self::ATT_VALUE);
    }

    /**
     * @param mixed $name
     * @return $this
     */
    public function setName($name)
    {
        $this->addAttribute(self::ATT_NAME, $name);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->getAttribute(self::ATT_NAME);
    }


    /**
     * Get filename for element template
     *
     * @return mixed
     */
    protected function getTemplate()
    {
        return 'input.html';
    }
}