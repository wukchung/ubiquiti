<?php

namespace Ubiquiti\Html\Tool;

class Path
{
    /**
     * Glue path pieces together with DIRECTORY SEPARATOR
     *
     * @param $fragment1
     * @param $fragment2
     * @return string
     */
    public static function glue($fragment1, $fragment2)
    {
        $args = array_map(function ($value) {
            return rtrim($value, DIRECTORY_SEPARATOR);
        }, func_get_args());

        return implode(DIRECTORY_SEPARATOR, $args);
    }
}