<?php

namespace Ubiquiti\Html;

use Ubiquiti\Html\Element\Div;
use Ubiquiti\Html\Element\Document;
use Ubiquiti\Html\Element\Form;
use Ubiquiti\Html\Element\Image;
use Ubiquiti\Html\Element\Input;
use Ubiquiti\Html\Element\Label;
use Ubiquiti\Html\Element\Link;
use Ubiquiti\Html\Element\Option;
use Ubiquiti\Html\Element\Paragraph;
use Ubiquiti\Html\Element\Select;
use Ubiquiti\Html\Element\Span;
use Ubiquiti\Html\Element\Text;

class ElementFactory
{
    /**
     * @return Div
     */
    public static function getDiv()
    {
        return new Div();
    }

    /**
     * @return Document
     */
    public static function getDocument()
    {
        return new Document();
    }

    /**
     * @return Form
     */
    public static function getForm()
    {
        return new Form();
    }

    /**
     * @return Image
     */
    public static function getImage()
    {
        return new Image();
    }

    /**
     * @return Input
     */
    public static function getInput()
    {
        return new Input();
    }

    /**
     * @return Label
     */
    public static function getLabel()
    {
        return new Label();
    }

    /**
     * @return Link
     */
    public static function getLink()
    {
        return new Link();
    }

    /**
     * @return Option
     */
    public static function getOption()
    {
        return new Option();
    }

    /**
     * @return Paragraph
     */
    public static function getParagraph()
    {
        return new Paragraph();
    }

    /**
     * @return Select
     */
    public static function getSelect()
    {
        return new Select();
    }

    /**
     * @return Select
     */
    public static function getSpan()
    {
        return new Span();
    }

    /**
     * @param $text
     * @return Select
     */
    public static function getText($text)
    {
        return new Text($text);
    }
}