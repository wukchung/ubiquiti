<?php

namespace Ubiquiti\Logger;

class LoggerFactory
{
    private static $logger;

    /**
     * Just some basic default logger
     */
    public static function get()
    {
        if (!self::$logger) {
            self::$logger = new UbiquitiLogger();
        }

        return self::$logger;
    }
}

